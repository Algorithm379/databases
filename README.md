# Databases

Database images to store and share specific libraries.

The libraries are generated using data from UniProt, Human Protein Atlas, and Disgenet.

Currently only human entries are supported.

Public  images contain:
- Disease association information
- Disease specificity information
- Cellular location information
- Protein class information
